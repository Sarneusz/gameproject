﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{

    public int health = 100;
    public ParticleSystem destroyedEffect;
    
    public void take_damage(int amount)
    {

        health -= amount;

        if (health <= 0)
        {
            Instantiate(destroyedEffect, transform.position, transform.rotation);
            Destroy(gameObject);
        }


    }
}
