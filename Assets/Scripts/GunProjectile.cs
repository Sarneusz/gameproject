﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GunProjectile : MonoBehaviour
{

        public int gun_damage = 10;
        public float shootforce, upwardForce;
        public float timeBetweenShooting, spread, reloadTime, timeBetweenShots;
        public int magazineSize, bulletsPerTap;
        public bool allowButtonHold;
        public GameObject bullet;
        public ParticleSystem muzzleFlash;
        public ParticleSystem hitEffect;
        public bool allowInvoke = true;
        
        public Image hitmarks;
        public Camera fpsCam;
        public Transform attackPoint;
        public AudioSource sfx;
        public AudioClip hitmarkSound;
        public AudioClip gunSound;
        public GameObject playerController;
        private Rigidbody rb;

        private Vector3 positionBullet;
        private bool isShooted = false;
        private Vector3 directionWithSpread;
        private bool shooting, readyToShoot, reloading;
        private int bulletsLeft;
        private float hitmarkerWait;
        
        private void Awake()
        {
                bulletsLeft = magazineSize;
                readyToShoot = true;
                
        }

        public void Start()
        {

                rb = playerController.GetComponent<Rigidbody>();

        }


        public void Update()
        {
                MyInput();
                
                //Hitmark
                if (hitmarkerWait > 0)
                {
                        hitmarkerWait -= Time.deltaTime;
                } 
                else if (hitmarks.color.a > 0)
                {
                        hitmarks.color = Color.Lerp(hitmarks.color, new Color(1, 1, 1, 0), Time.deltaTime * 3f);
                }
        }

        private void MyInput()
        {
                if (allowButtonHold)
                        shooting = Input.GetKey(KeyCode.Mouse0);
                else
                        shooting = Input.GetKeyDown(KeyCode.Mouse0);


                if (readyToShoot && shooting && !reloading && bulletsLeft > 0)
                {
                        Shoot();
                }
        
                if(Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading)
                        Reload();
                
                if(readyToShoot && shooting && !reloading && bulletsLeft <= 0) 
                        Reload();
        }

        
        private void Shoot()
        {
                isShooted = true;
                readyToShoot = false;
                sfx.PlayOneShot(gunSound);                
                muzzleFlash.Play();                
                Ray ray = fpsCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                RaycastHit hit;

                Vector3 targetPoint;
                //Hit target
                if (Physics.Raycast(ray, out hit))
                {
                        targetPoint = hit.point;
                        Instantiate(hitEffect, targetPoint, Quaternion.LookRotation(hit.normal));

                        Target target = hit.transform.GetComponent<Target>();
                        //Hit target has script Target
                        if (target) 
                        {
                                target.take_damage(gun_damage);
                                hitmarkerWait = 0.5f;
                                hitmarks.color = Color.white;
                                sfx.PlayOneShot(hitmarkSound);
                        }

                }
                else
                        targetPoint = ray.GetPoint(75);

                Vector3 directionWithoutSpread = targetPoint - attackPoint.position;

                float x = Random.Range(-spread, spread);
                float y = Random.Range(-spread, spread);
                directionWithSpread = directionWithoutSpread + new Vector3(x, y, 0);


                positionBullet = rb.velocity * Time.deltaTime;

                
                bulletsLeft--;

                if (allowInvoke)
                {
                        Invoke("ResetShot", timeBetweenShooting);
                }
                
        }

        public void LateUpdate()
        {
                if (isShooted)
                {
                        GameObject currentBullet = Instantiate(bullet, attackPoint.position + positionBullet,
                                Quaternion.identity);

                        currentBullet.transform.forward = directionWithSpread.normalized;

                        currentBullet.GetComponent<Rigidbody>().velocity = currentBullet.transform.forward * shootforce;
                        isShooted = false;
                }
        }

        private void ResetShot()
        {
                readyToShoot = true;
                allowInvoke = true;
        }

        private void Reload()
        {
                reloading = true;
                Invoke("ReloadFinished", reloadTime);
        }

        private void ReloadFinished()
        {
                bulletsLeft = magazineSize;
                reloading = false; 
        }
}
